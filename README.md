# Programmability Workshop - August 2018

Welcome to the 2-day programmability workshop hosted by Cisco. We will be using the following documentation over the course of the hands-on excercises, as well as using mostly modules from https://learninglabs.cisco.com/tracks/devnet-express-dci. 

### [Lab Setup](0-Setup.md)

<!-- ### [YAML Basics](1-yaml.md)

### [Lab 1 - NXOS](https://learninglabs.cisco.com/tracks/devnet-express-dci/dne-dci-intro-nx-os/dne-dci-intro-nxos-06_ansible-nxapi/step/2)

### [Lab 2 - NXOS](https://learninglabs.cisco.com/tracks/devnet-express-dci/dne-dci-intro-nx-os/dne-dci-intro-nxos-06_ansible-nxapi/step/4)
 -->

---

## Ansible for IOS XE Module: 

From Ubuntu: 
1. git clone https://github.com/CiscoDevNet/dnav3-code
1. cd dnav3-code
1. git checkout intro-ansible

### [Draft Module](https://learninglabs.cisco.com:8867/tracks/dnav3-draft-track)

--or-- 

### [Lab 1 - IOS-XE](intro-ansible-iosxe-labs/ansible-ios-modules/1-intro.md)

### [Lab 2 - IOS-XE](intro-ansible-iosxe-labs/ansible-netconf/1-intro.md)

### [Mission 1 - IOS-XE](intro-ansible-iosxe-labs/ansible-mission/1-intro.md)

---
