# Step 3: Understand a sample playbook structure
Playbooks are YAML documents that contain a set of tasks to be performed by Ansible. Within this section, we will review the key components of a playbook before getting into IOS-specific plays in the next section.

Below is an example playbook that we will talk through the components of one at a time:

```yaml
#simple IOS config in ansible
---
- name: Sample IOS config for Ansible 2.5
  hosts: iosxe

  tasks:
  - name: set ACL via CLI
    ios_config:
      lines:
        - 10 permit ip host 1.1.1.1 any log
        - 20 permit ip host 2.2.2.2 any log
        - 30 permit ip host 3.3.3.3 any log
        - 40 permit ip host 4.4.4.4 any log
        - 50 permit ip host 5.5.5.5 any log
      parents: ['ip access-list extended pod_{{user_pod}}_acl']
      before: no ip access-list extended pod_{{user_pod}}_acl

  - name: Confirm all VRFs exist
    ios_vrf:
      vrfs: "{{ local_vrfs }}"
      state: present
      purge: yes
    check_mode: yes

```

The above playbook is intended to create an access-list on the devices, and confirm that a specific set of VRFs exist on those devices. Let's review how it accomplishes that.

## Plays
In Ansible, each playbook is made up of one or more plays. As you already learned, inventory files contain a list of hosts and groups that Ansible will manage. Each play that is created will reference a specific group from the inventory, and will apply one or more tasks to that group of devices.  
```yaml
---
- name: Sample IOS config for Ansible 2.5
  hosts: iosxe
  gather_facts: no

  tasks:

```
You can see in this example we have created a single play.  As you learned in the introduction to YAML, a single dash in Ansible, `-`, defines a single item in a list. Inside of a playbook, each play is a unique item in a list of plays, so you could easily add additional plays by starting each with a single dash, as shown below:

```yaml
---
- name: Sample IOS config for Ansible 2.5
  hosts: routers-primary
  tasks:

- name: A Second play
  hosts: routers-secondary
  tasks:
```
In this example, you can see that the first play is designed to apply a set of tasks (not shown) to device group `routers-primary`. Once that completes, it moves on to a second play which will apply a different set of tasks to device group `routers-secondary`. A series of plays could be used to ensure that only one half of a redundant pair of devices is affected at a time when making changes.

## Name
Name is an optional component, but it is a good practice to begin each play with it as a friendly description of what this play is meant to accomplish.
```yaml
- name: Sample IOS config for Ansible 2.5
```

## Hosts
In every playbook, we must define which hosts we want it to apply to. In this example, our inventory file has already defined a group called `iosxe` which contains a list of the routers we will be managing. If we instead wanted the play to only affect one host, we could list it instead.
```yaml
  hosts: iosxe
```

## Tasks
Once we have defined which hosts a play will apply to, we move on to defining tasks.
```yaml
  tasks:
  - name: set ACL via CLI
  - name: Confirm all VRFs exist
```
The above line creates a list of tasks to be completed. Remember that YAML treats each ``-`` as a new list item, so be sure to preface each unique task with a ``-`` at the proper indentation level. As with each play, a friendly name for each task is highly encouraged to self-document your work.

## Modules
Every task in Ansible uses a module to execute. Ansible ships with a large number of pre-defined and supported modules, but users are free to create their own for custom tasks that do not already exist. Users can also find third party modules for many common tasks on places like GitHub. Before building your own module, it is a good idea to search for one already published by someone else as a starting point.

```yaml
  - name: set ACL via CLI
    ios_config:
  - name: Confirm all VRFs exist
    ios_vrf:
```
In the above example, we have two tasks, each one using a different module. The first uses the `ios_config` module, and the second uses the `ios_vrf` module. The next section will go into more detail on many of the common IOS modules and how they work.

## Parameters
Each module has a pre-defined set of parameters, including some mandatory and some optional parameters. Most parameters also have default values to simplify the amount of config necessary in a play.

```yaml
  - name: set ACL via CLI
    ios_config:
      lines:
        - 10 permit ip host 1.1.1.1 any log
        - 20 permit ip host 2.2.2.2 any log
        - 30 permit ip host 3.3.3.3 any log
        - 40 permit ip host 4.4.4.4 any log
        - 50 permit ip host 5.5.5.5 any log
      parents: ['ip access-list extended pod_{{user_pod}}_acl']
      before: no ip access-list extended pod_{{user_pod}}_acl

  - name: Confirm all VRFs exist
    ios_vrf:
      vrfs: "{{ local_vrfs }}"
      state: present
      purge: yes
    check_mode: yes
```
In the above example, `ios_config` has a parameter of `lines` which expects a list of config commands to be applied to the device. The `ios_config` module will iterate through each item in the list and attempt to apply that config to the hosts defined for this play. 

In `ios_vrf`, there are several important parameters. 
* `vrf` expects a list of VRFs which it will act upon. 
* The `state` parameter specifies if the VRF should be present or not. If set to `present`, it will create any missing VRFs. If set to `absent`, it will remove the listed VRFs. 
* Last, the `purge` parameter tells this module that if it is given a list of VRFs that should be present, it should remove any VRFs from the device that are not in the list.

There are two additional parameters that are frequently needed when performing device configs with `ios_config` due to IOS's hierarchical command structure.

### Parents
The `parents` parameter allows users to define commands that the `lines` must be executed within. In the above example, once in config mode, the module must first enter ACL config mode with an `ip access-list` command before it is able to add specific ACE entries to the ACL.

### Before
The `before` parameter allows users to run specific config commands prior to adding the new config. In the ACL example, it may be desirable to remove any existing ACL with the same name before creating a new one. The before command is being used to remove any existing ACL with the same name before applying the desired config.

## Documentation
In order to see the latest parameters for any module, please refer to the Ansible documentation either at the [Module Index](https://docs.ansible.com/ansible/latest/modules/modules_by_category.html) or at the command line by running "ansible-doc <modulename>".

## Conditionals
When defining plays, it is sometimes necessary to further limit which devices the specific task will apply to. For example, specific tasks may only apply to devices with a specific version of IOS, or a specific type of device. In these cases, we can apply a conditional statement to a task.

```yaml
  - name: GATHERING FACTS
    ios_facts:
      gather_subset: hardware
  - name: Confirm all VRFs exist
    ios_vrf:
      vrfs: "{{ local_vrfs }}"
      state: present
      purge: yes
    when: ansible_net_version != "16.08.01a"
```

In the above example, the task would only execute against devices that are not running a specific running version of IOS, as discovered by the `ios_facts` module in the previous task.

## Loops
Within tasks, it is often useful to be able to iterate through a list of items. For example, you might want to define a list of subnets and loop through each setting NTP to point to a server on that subnet. In these cases, you can define a list just as you would for any list in Ansible.

```yaml
loops:
  - 11
  - 12
  - 13
  - 14
```
In this example, we define 4 list items within list `loops` within a variable file `iosxe.yml`. We then want to utilize the list as part of a task that defines NTP servers.

```yaml
  tasks:

  - name: set ntp server 10.{{item}}.{{pod_number}}.65 via CLI
    ios_config:
      lines:
        - ntp server 10.{{item}}.{{pod_number}}.65
    with_items: "{{loops}}"

```
In the above task, we use the Ansible keyword `with_items:` to indicate that the task should loop through a list. In this case, we tell it to run the task once per item in the list. Within the task, in order to capture the value of each list item, we use the variable `{{item}}`. So you can see, during the first loop it will add a server with IP address `10.11.pod_number.65`, and on the second loop `10.12`, third `10.13`, etc.


## Next step

Proceed to Step 3: Understand the IOS Modules in Ansible.
