# Summary

In this Learning Lab, we've explored the fundamental concepts of Ansible and how they apply to networking devices. We learned that when managing networking devices, Ansible does not rely on bundling up the tasks and modules and executing on the remote device, but rather it executes on the control station and uses SSH or API to then connect to the devices. 

We also showed some examples of playbook layouts, and reviewed several of the native IOS modules that ship with Ansible. In the next labs in this module we will test several of the native modules to learn how they work, as well as exploring the NETCONF module as an alternative option for managing devices.
