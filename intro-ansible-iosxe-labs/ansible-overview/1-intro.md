# Introduction to Ansible for IOS XE
This module introduces the basic constructs of Ansible as a configuration management tool, as well as specific modules designed for IOS XE. It will also cover a module that can apply NETCONF configurations onto multiple device types, offering some added efficiency for multi-platform environments.


## Objectives

1. Understand the basic building blocks used when managing devices with Ansible.
2. Understand how to leverage the built-in IOS XE modules in Ansible to manage IOS XE-based switching and routing platforms.
3. Understand the Ansible `netconf_config` module, and how it can be used to configure IOS XE devices.


## Prerequisites
To complete this lab you need:

* A workstation running Linux or OSX. Although it can be used to manage Windows, Ansible does not run on Windows as the control station. If your development workstation is Windows, you can load a Linux VM, a Linux container, or connect via SSH to another Linux workstation as your control station. If you do not have a workstation available, you can reserve a Devnet [Devbox sandbox](https://devnetsandbox.cisco.com/RM/Diagram/Index/f1a51f3b-3377-444d-97f0-5ad300d976be?diagramType=Topology)
* A development environment with Ansible 2.5 and a compatible version of Python.  If you are at a DevNet Event using a provided workstation, you are ready to go.  If you are working from your own workstation, please review the ***"How to setup your own computer"*** link at the top of this page.  
* Lab infrastructure to target API calls and code.  These labs and code examples are written to leverage the [DevNet IOS XE Always On Sandbox](https://devnetsandbox.cisco.com/RM/Diagram/Index/27d9747a-db48-4565-8d44-df318fce37ad?diagramType=Topology).  This lab is available for anyone to use, with only access to the Internet as a requirement. To use a different device, ensure the device is running IOS XE 16.6 or higher.

You should also have an understanding of these foundational topics:
* Basic ability to read and understand Python code samples and scripts. You can explore the **"Programming Fundamentals"** Learning Labs available on DevNet.
* Ability to read and understand the NETCONF configuration example. You can explore the **"Introduction to Model Driven Programmability"** Module available on DevNet for an introduction to NETCONF.
* Students are encouraged to have a basic understanding of Ansible. This Module will cover some basics, but students requiring additional background are encouraged to review [Introduction to Ansible](https://learninglabs.cisco.com/modules/sdx-ansible-intro).

## Next step

Proceed to Step 1: Understand YAML.
