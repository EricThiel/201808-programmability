# Understand the IOS Modules in Ansible
Ansible ships with many modules included in each release. As of 2.5, there are a variety of built-in modules for managing Cisco devices, including over a dozen focused exclusively on IOS XE devices.

![](assets/images/ios_modules1.png)

An up-to-date list is available at http://docs.ansible.com/ansible/latest/modules/list_of_network_modules.html#ios, and it changes as new modules are submitted by the community for inclusion in the next release. In addition to the official modules, there are numerous modules developed by third parties that are available for download if there is a gap in the official module capabilities.

Let's review some of the most common IOS modules and how to work with them. Again, due to frequent changes it is advisable to review `docs.ansible.com` or `ansible-docs` command line tool to see the latest information for each specific module you work with.


## ios_facts
The `ios_facts` module gathers basic facts about the remote IOS device it is connecting to. This information can in turn be used as a variable or in a conditional statement later in the playbook. The data this module collects will be stored as variables with the prefix `ansible_net_`.

```yaml
---
- name: Sample IOS show version for Ansible 2.5
  hosts: iosxe
  gather_facts: no

  tasks:

  - name: GATHERING FACTS
    ios_facts:
      gather_subset: hardware

  - name: Devices without version 16.08.01a
    debug:
      var: ansible_net_version
    when: ansible_net_version != "16.08.01a"
```
In this play, we gather the version of each device. We then check the `ansible_net_version` variable, which was set by `ios_facts`, and if it is not equal to `16.08.01a` we print out the current running version.

Taking it a step at a time, we start our play with a single `-` to define the beginning of a play.
```yaml
-
```

Next, we tell the play to execute against all hosts in group `iosxe`.
```yaml
  hosts: iosxe
```

We then tell it we will be running a set of `tasks:`.
```yaml
  tasks:
```

Our first task is calling the `ios_facts` module that is included in Ansible by default:
```yaml
    ios_facts:
```
We then tell it to only gather hardware details about each platform:
```yaml
      gather_subset: hardware
```

At this point, the `ios_facts` module will save all of the hardware details about the platform into variables that can be referenced by other tasks.

Next we call the `debug` module, which is also included in Ansible by default and is often used for logging output to the console:
```yaml
    debug:
```
And with the `debug` module we will print out the IOS version we learned from `ios_facts`.
```yaml
      var: ansible_net_version
```
But, with the `when` condition, we are choosing to execute this debug output only when the detected version is not `16.08.01a`:
```yaml
    when: ansible_net_version != "16.08.01a"
```


## ios_command
The `ios_command` module is designed to send arbitrary commands to an IOS device and retrieve the response. This module is commonly used to gather output from `show` commands and store it in variables.

```yaml
  - name: run show version and check to see if output contains IOS
    ios_command:
      commands: show version
      wait_for: result[0] contains IOS
    register: myver

  - debug:
      var: myver
```
In this example we run a `show version`, store the output as `myvar`, and print the output. Once it is stored in a variable, we could also reference the contents of the variable in future plays.

It is worth noting, as you will see in subsequent labs, the output of this module is a representation of the multi-line CLI output. Because it is not a native data structure, it can be more difficult to work with than the output of something like `ios_facts`, which returns much of the same information as individual variables such as `ansible_net_version`.


## ios_config

```yaml
  tasks:
  - name: Base config template
    ios_config:
      lines:
        - logging buffered 10240
        - service timestamps debug datetime msec localtime show-timezone
        - service timestamps log datetime msec localtime show-timezone
```
In this example we use the `ios_config` module to apply several config commands to the device. The `ios_config` module is idempotent for many, but not all commands. As a result, it is generally best to choose a more specific module if it is available. A good example would be `ios_user` instead of creating users with `ios_config`.


## ios_vrf
vars.yml:
```yaml
local_vrfs:
  - red
  - blue
  - green
```

As you can see, we start by setting up a `local_vrfs` list that currently has 3 VRFs defined in it. This definition could be whatever is most reusable, whether that is a global variable file, a group variable file, or a host variable file.

vrfs.yml:

```yaml
  tasks:

  - name: Confirm all VRFs exist
    ios_vrf:
      vrfs: "{{ local_vrfs }}"
      state: present
      purge: yes
    check_mode: yes
```

In this example we are using the `ios_vrf` module to declare which VRFs we want to exist on the router. Because we use the optional `purge: yes` parameter, the `ios_vrf` module will delete any additional VRFs from the device that are not included in ``{{ local_vrfs }}`` address defined in our variable file.

```yaml
      vrfs: "{{ local_vrfs }}"
```
Here we pass a list variable that contains a list of VRFs as a parameter to `ios_vrf`.

```yaml
      state: present
```
This line specifies that we want to ensure these VRFs exist. In contrast, `state: absent` would delete this list of VRFs.

```yaml
      purge: yes
```
We are telling the module that the list of VRFs in `local_vrfs` should be a complete list, and any additional VRFs that exist on the device should be removed.

```yaml
    check_mode: yes
```
This is a useful parameter that tells the task to only evaluate what it would do, without taking action. It is very useful for testing new script changes or just for building a script to audit devices. It behaves similar to the ``-C`` command-line option, but can be specified on individual tasks unlike ``-C``.

## ios_user

vars.yml:
```yaml
local_users:
  cisco:
    priv: 15
    password: "{{password}}"
  admin:
    priv: 15
    password: "{{password}}"
```

users.yml:
```yaml
  tasks:

  - name: Configure cisco users
    with_items: "{{ local_users }}"
    ios_user:
      name: "{{item}}"
      privilege: "{{ item.priv | default(15) }}"
      configured_password: "{{ item.password | default('password') }}"
      state: present
```

In this example, we leverage a variable named `{{local_users}}` created in a group variable file. The play loops through all users within the list and creates a user with the correct privilege and password. If not specified, privilege defaults to `15` and password to `password`.

## ios_l3_interface

```yaml
  - name: Set GigabitEthernet0/1 IP address
    ios_l3_interface:
      name: GigabitEthernet0/1
      ipv4: 10.11.12.13/24

```

In this example, we are using the `ios_l3_interface` module to configure an IP address on a specific interface, `GigabitEthernet0/1`. The above format is useful, especially when looping through a list of interfaces and IPs that may be stored in a variable file.


## netconf_config
The last module we will cover, `netconf_config`, is unique in that it is not specific to one platform. The `netconf_config` module can talk to any NETCONF-capable device, allowing for more universal tasks across multiple platforms. In the following example, we use NETCONF to define an NTP server on a router.

```yaml
---
- name: NETCONF create NTP server
  hosts: iosxe
  connection: local
  gather_facts: False

  tasks:

    - set_fact:
        ansible_connection: local

    - name: Define NTP server 10.111.{{pod_number}}.66 with NETCONF
      netconf_config:
        host: "{{inventory_hostname}}"
        port: "{{netconf_port}}"
        username: "{{ansible_user}}"
        password: "{{ansible_password}}"
        hostkey_verify: False
        xml: |
            <config xmlns:xc="urn:ietf:params:xml:ns:netconf:base:1.0">
              <native xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native" xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0">
                <ntp>
                  <server xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-ntp">
                    <server-list>
                      <ip-address>10.111.{{pod_number}}.66</ip-address>
                    </server-list>
                  </server>
                </ntp>
              </native>
            </config>
```

```yaml
  hosts: iosxe
  connection: local
```
As with past examples, we apply this play to the group `iosxe`.
```yaml
    - set_fact:
        ansible_connection: local
```
Unlike the `ios_` modules which support connection type `network_cli`, starting in Ansible 2.5, the `netconf_config` module currently relies on setting your ansible_connection type to `local`. Because we set type to `network_cli` globally, we override that value for this playbook with `set_facts`.

```yaml
      netconf_config:
        host: "{{inventory_hostname}}"
        port: "{{netconf_port}}"
        username: "{{ansible_user}}"
        password: "{{ansible_password}}"
        hostkey_verify: False
```
Because it is built on SSH connectivity, the `netconf_config` module requires that you to specify the host, port, username, and password.

```yaml
        xml: |
            <config xmlns:xc="urn:ietf:params:xml:ns:netconf:base:1.0">
              <native xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native" xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0">
                <ntp>
                  <server xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-ntp">
                    <server-list>
                      <ip-address>10.111.{{pod_number}}.66</ip-address>
                    </server-list>
                  </server>
                </ntp>
              </native>
            </config>
```
Lastly, the `netconf_config` module requires an XML block with the config to be sent to the device. For background on how to build the XML config, please review the Introduction to Model Driven Programmability Module.


## Next step

Proceed to the following Learning Lab: Ansible IOS native modules.
