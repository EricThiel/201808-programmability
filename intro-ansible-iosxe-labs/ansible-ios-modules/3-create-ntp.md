# Define an NTP server

## Hands-on task:

1. Open a terminal, change to the root of the code samples repository, and make sure your virtual environment is active.
1. Change into the directory for this lab.  

    ```bash
    cd intro-ansible/
    ```

1. Within the directory is a folder called `ansible-02-ios-modules` that contain several sample playbooks.

    ```bash
    ls ansible-02-ios-modules
    ```

    <details>
    <summary> Expected Output </summary>
    <pre><code>
    02-ios_command_show.yaml
    02-ios_command_show_ntp.yaml
    02-lab_cleanup.yaml
    02-loopback_create.yaml
    02-loopback_delete.yaml
    02-ntp_create.yaml
    02-ntp_delete.yaml
    </code></pre>
    </details>

1. Within the directory is a file called `hosts` that contain both the device inventory, as well as a few key variables.

    ```bash
    cat hosts
    ```

    <details>
    <summary> Expected Output </summary>
    <pre><code>
        [all:vars]
        ansible_python_interpreter="/usr/bin/env python"

        [iosxe:vars]
        # Fill in a unique pod name with no spaces
        user_pod = mypod
        # Fill in a pod number between 10 and 250
        pod_number = 10
        # Fill in your Webex Teams auth token from https://developer.webex.com/getting-started.html
        webex_token = mytokenabcdef
        # Fill in your Webex Teams room name you wish to post to
        webex_room_id = roomIDabc123456defghi789jklmnopqr0

        [iosxe:children]
        # FYI - If you are in a DevNet Express please add a #
        # in front of sandbox and remove the # in front of express.
        sandbox
        #express

        [sandbox]
        ios-xe-mgmt.cisco.com ansible_port=8181

        [express]
        198.18.134.11  # dcloud pod router #1
        198.18.134.12  # dcloud pod router #2 
    </code></pre>
    </details>

1. Edit the hosts file and fill in `user_pod` with a string for your pod name, and a number for your `pod_number` between 10 and 250.

1. Save and exit the hosts file.

1. Execute the following playbook to confirm the list of NTP servers:

    ```bash
    ansible-playbook ansible-02-ios-modules/02-ios_command_show_ntp.yaml
    ```
    <details>
    <summary> Sample Output </summary>
    <pre><code>
        PLAY [Sample IOS show ntp for Ansible 2.5] *******************************************************************************************************

        TASK [run show ntp associations] *********************************************************************************************************************
        ok: [ios-xe-mgmt.cisco.com]

        TASK [display value of "myint" variable] *************************************************************************************************************
        ok: [ios-xe-mgmt.cisco.com] => {
            "myntp[\"stdout_lines\"][0]": [
                "address         ref clock       st   when   poll reach  delay  offset   disp",
                " ~10.34.45.56     .INIT.          16      -   1024     0  0.000   0.000 15937.",
                " ~1.1.1.2         .INIT.          16      -   1024     0  0.000   0.000 15937.",
                " * sys.peer, # selected, + candidate, - outlyer, x falseticker, ~ configured"
            ]
        }

        PLAY RECAP *******************************************************************************************************************************************
        ios-xe-mgmt.cisco.com      : ok=2    changed=0    unreachable=0    failed=0
    </code></pre>
    </details>

1. Observe in the output a list of current NTP servers defined on the router. Make a mental note of which servers currently are defined.
    * Note: If you see existing servers defined with 10.X.`<pod_number>`.65 or 66, this means someone has previously used your pod number for a lab. We suggest choosing a new number in your hosts file to avoid confusion.

1. Execute the following playbook to add a new NTP server on your router:

    ```bash
    ansible-playbook ansible-02-ios-modules/02-ntp_create.yaml
    ```

    <details>
    <summary> Sample Output </summary>
    <pre><code>
        PLAY [Sample IOS NTP config for Ansible 2.5] *********************************************************************************************************

        TASK [set ntp server 10.{{item}}.{{pod_number}}.65 via CLI] ****************************************************************
        changed: [ios-xe-mgmt.cisco.com] => (item=11)
        changed: [ios-xe-mgmt.cisco.com] => (item=12)
        changed: [ios-xe-mgmt.cisco.com] => (item=13)
        changed: [ios-xe-mgmt.cisco.com] => (item=14)

        PLAY RECAP *******************************************************************************************************************************************
        ios-xe-mgmt.cisco.com      : ok=1    changed=1    unreachable=0    failed=0
    </code></pre>
    </details>

1. Execute the following playbook to confirm the new list of NTP servers:

    ```bash
    ansible-playbook ansible-02-ios-modules/02-ios_command_show_ntp.yaml
    ```

1. Confirm 4 NTP servers have been added as 10.`<loop_number>`.`<pod_number>`.65 where `loop_number` is 11, 12, 13, and 14. 

1. Open up ```ansible-02-ios-modules/02-ntp_create.yaml``` in a text editor and review the contents.

 * Can you identify which module was used to configure NTP on the server?
    <details>
    <summary> Answer </summary>
    This lab used the `ios_config` module to execute specific config commands directly onto the device
    </details>

 * If you wanted to use that module to make configuration changes, where would you go for information about mandatory and optional parameters, as well as examples of usage?
    <details>
    <summary> Answer </summary>
    https://docs.ansible.com/ansible/latest/modules/ios_config_module.html#ios-config-module
    execute `ansible-doc ios_config` on your ansible workstation
    </details>

 * What line tells Ansible to loop through multiple variables? How many times does the playbook execute that task that is using loops?
    <details>
    <summary> Answer </summary>
    `with_items: {{loops}}` tells the playbook to execute that task once for every item in list `loops` (which is defined in the `iosxe.yml` variable file).
    For each item in `loops`, the task executes once with the variable `items` set to the value of that list item.
    </details>

## Next step

Proceed to Step 3: [Create a loopback.](4-create-loopback.md)
