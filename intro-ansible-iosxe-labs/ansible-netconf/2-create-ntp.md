# Define NTP with NETCONF

## Hands-on task:

1. Open a terminal, change to the root of the code samples repository, and make sure your virtual environment is active.
1. Change into the directory for this lab.  

    ```bash
    cd intro-ansible/
    ```

1. Within the directory is a folder called `ansible-03-netconf-config` that contains several sample playbooks.

    ```bash
    ls ansible-03-netconf-config
    ```

    <details>
    <summary> Expected Output </summary>
    <pre><code>
    03-lab_cleanup.yaml
    03-loopback_create.yaml
    03-loopback_delete.yaml
    03-ntp_create.yaml
    03-ntp_delete.yaml
    </code></pre>
    </details>

1. Within the directory is a file called `hosts` that contains both the device inventory, as well as a few key variables.

    ```bash
    cat hosts
    ```

    <details>
    <summary> Expected Output </summary>
    <pre><code>
    [all:vars]
    ansible_python_interpreter="/usr/bin/env python"

    [iosxe:vars]
    # Fill in a unique pod name with no spaces
    user_pod = mypod
    # Fill in a pod number between 10 and 250
    pod_number = 10
    # Fill in your Webex Teams auth token from https://developer.webex.com/getting-started.html
    webex_token = mytokenabcdef
    # Fill in your Webex Teams room name you wish to post to
    webex_room_id = roomIDabc123456defghi789jklmnopqr0

    [iosxe:children]
    # FYI - If you are in a DevNet Express please add a #
    # in front of sandbox and remove the # in front of express.
    sandbox
    #express

    [sandbox]
    ios-xe-mgmt.cisco.com ansible_port=8181

    [express]
    198.18.134.11  # dcloud pod router #1
    198.18.134.12  # dcloud pod router #2 
    </code></pre>
    </details>

1. Edit the hosts file and fill in `user_pod` with a string for your pod name, and a number for your `pod_number` between 10 and 250.

1. Save and exit the hosts file.

1. Execute the following playbook to confirm the list of NTP servers:

    ```bash
    ansible-playbook ansible-02-ios-modules/02-ios_command_show_ntp.yaml
    ```
    <details>
    <summary> Sample Output </summary>
    <pre><code>
    PLAY [Sample IOS show ntp for Ansible 2.5] *******************************************************************************************************

    TASK [run show ntp associations] *********************************************************************************************************************
    ok: [ios-xe-mgmt.cisco.com]

    TASK [display value of "myint" variable] *************************************************************************************************************
    ok: [ios-xe-mgmt.cisco.com] => {
        "myntp[\"stdout_lines\"][0]": [
            "address         ref clock       st   when   poll reach  delay  offset   disp",
            " ~10.34.45.56     .INIT.          16      -   1024     0  0.000   0.000 15937.",
            " ~1.1.1.2         .INIT.          16      -   1024     0  0.000   0.000 15937.",
            " * sys.peer, # selected, + candidate, - outlyer, x falseticker, ~ configured"
        ]
    }

    PLAY RECAP *******************************************************************************************************************************************
    ios-xe-mgmt.cisco.com      : ok=2    changed=0    unreachable=0    failed=0
    </code></pre>
    </details>

1. Observe in the output a list of current NTP servers defined on the router. Make a mental note of which servers currently are defined.
    * Note: If you see existing servers defined with `10.111.pod_number.65` or `66`, this means someone has previously used your pod number for a lab. We suggest choosing a new number in your hosts file to avoid confusion.

1. Execute the following playbook to add a new NTP server on your router:

    ```bash
    ansible-playbook ansible-03-netconf-config/03-ntp_create.yaml
    ```

    <details>
    <summary> Sample Output </summary>
    <pre><code>
    PLAY [NETCONF create NTP server] *********************************************************************************************************************

    TASK [set_fact] **************************************************************************************************************************************
    ok: [ios-xe-mgmt.cisco.com]

    TASK [Define NTP server 10.111.10.66 with NETCONF] ***************************************************************************************************
    changed: [ios-xe-mgmt.cisco.com]

    PLAY RECAP *******************************************************************************************************************************************
    ios-xe-mgmt.cisco.com      : ok=2    changed=1    unreachable=0    failed=0
    </code></pre>
    </details>

1. Execute the following playbook to confirm the new list of NTP servers:

    ```bash
    ansible-playbook ansible-02-ios-modules/02-ios_command_show_ntp.yaml
    ```

1. Confirm your NTP server has been added as `10.111.<pod_number>.66`.

1. Open up ```ansible-03-netconf-config/03-ntp_create.yaml``` in a text editor and review the contents.

 * Can you identify which module was used to configure NTP on the server?
    <details>
    <summary> Answer </summary>
    This lab used the `netconf_config` module to apply config changes formatted in XML directly onto the device with NETCONF

    </details>
 * If you wanted to use that module to make configuration changes, where would you go for information about mandatory and optional parameters, as well as examples of usage?
    <details>
    <summary> Answer </summary>
    https://docs.ansible.com/ansible/latest/modules/netconf_config_module.html#netconf-config-module
    execute `ansible-doc netconf_config` on your ansible workstation

    </details>
 * Why would you potentially use netconf instead of native IOS modules for configuration changes?
    <details>
    <summary> Answer </summary>
    There are multiple reasons why people may choose NETCONF or native modules. NETCONF can be very useful in heterogeneous environments with multiple platforms to manage. When consistent YANG data models are supported by all of the platforms in use, a single playbook could easily manage a diverse set of infrastructure devices. If this is compelling to you, please check out the Model-Driven Programmability module for more information on how to build XML files for use with NETCONF.

    In contrast, the IOS native modules are more human-readable and intuitive for many users because they do not require XML formatting. If your environment does not contain multiple different platforms, the native modules provide a very easy to use option with well documented parameters.
    
    </details>

## Next step

Proceed to Step 2: [Create a loopback interface with NETCONF.](3-create-loopback.md)
