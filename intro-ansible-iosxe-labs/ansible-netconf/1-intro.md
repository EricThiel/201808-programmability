# Ansible NETCONF modules with IOS XE routers

This lab exercise will get you started using NETCONF module that is included in Ansible 2.5 and later to interact with IOS XE routers. Additional information about the module can be found at [https://docs.ansible.com/ansible/latest/modules/netconf_config_module.html#netconf-config-module](https://docs.ansible.com/ansible/latest/modules/netconf_config_module.html#netconf-config-module).

## Objectives

The objective of this lab is to show how to:

* Use the NETCONF module to define NTP server on an IOS XE router.
* Use the NETCONF module to create loopback and assign IP on an IOS XE router.

## Prerequisites
To complete this lab you need:

* A workstation running Linux or OSX. Although it can be used to manage Windows, Ansible does not run on Windows as the control station. If your development workstation is Windows, you can load a Linux VM, a Linux container, or connect via SSH to another Linux workstation as your control station. If you do not have a workstation available, you can reserve a Devnet [Devbox sandbox](https://devnetsandbox.cisco.com/RM/Diagram/Index/f1a51f3b-3377-444d-97f0-5ad300d976be?diagramType=Topology)
* A development environment with Ansible 2.5 and a compatible version of Python.  If you are at a DevNet Event using a provided workstation, you are ready to go.  If you are working from your own workstation, please review the ***"How to setup your own computer"*** link at the top of this page.  
* Lab infrastructure to target API calls and code.  These labs and code examples are written to leverage the [DevNet IOS XE Always On Sandbox](https://devnetsandbox.cisco.com/RM/Diagram/Index/27d9747a-db48-4565-8d44-df318fce37ad?diagramType=Topology).  This lab is available for anyone to use, with only access to the Internet as a requirement. To use a different device, ensure the device is running IOS XE 16.6 or higher.

You should also have an understanding of these foundational topics:
* The previous Learning Labs in the **"Introduction to Ansible for IOS XE"** Module.  
* Ability to read and understand the NETCONF configuration example. You can explore the **"Introduction to Model Driven Programmability"** Module available on DevNet for an introduction to Netconf.
* Students are encouraged to have a basic understanding of Ansible. This Module will cover some basics, but students requiring additional background are encouraged to review [Introduction to Ansible](https://learninglabs.cisco.com/modules/sdx-ansible-intro).

## Next step

Proceed to Step 1: [Define NTP with NETCONF.](2-create-ntp.md)
